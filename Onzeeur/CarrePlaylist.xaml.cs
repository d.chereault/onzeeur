﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using MySql.Data.MySqlClient;

namespace Onzeeur
{
    /// <summary>
    /// Logique d'interaction pour CarrePlaylist.xaml
    /// </summary>
    public partial class CarrePlaylist : UserControl
    {
        private int id;
        public CarrePlaylist(string name, string image, int id)
        {
            InitializeComponent();
            this.DataContext = this;
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri(image));
            BtnPlayMusic.Background = brush;
            lblTitle.Text = name;
            this.id = id;
        }

        #region Properties

        private static string _imageCP;
        private static string _titleCP;
        

        public static string ImageCP
        {
            get { return _imageCP; }
            set { _imageCP = value; }
        }

        public static string TitleCP
        {
            get { return _titleCP; }
            set { _titleCP = value; }
        }


        #endregion

        private void BtnPlay(object sender, RoutedEventArgs e)
        {
            if (!PageAccueil.Instance.Menu.Items.Contains("AffichageListMusique"))
            {
                List<AffichageMusique> listMusic = new List<AffichageMusique>();
                listMusic = SqlContent.ListMusiqueTitres(id);
                PageAccueil.Instance.Menu.ItemsSource = listMusic;
                PageAccueil.setListMusic(listMusic);
            }
        }
    }
}
