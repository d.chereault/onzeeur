﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Media;
using WMPLib;

namespace Onzeeur
{
    class SqlContent
    {
        static MySqlDataReader LogoApp;
        static string value;
        static MySqlDataReader ListPlaylist;
        static MySqlDataReader connexionMDP;
        static MySqlDataReader ListCarre;
        static MySqlDataReader ListMusique;
        static MySqlDataReader ListSearchmusic;

        public static void Image(string libelle)
        {
            
            using (MySqlConnection connection = new MySqlConnection(ConnexionSQL.ConnStr))
            {
                string query = "Select source From image where libelle=@lbl";

                using (MySqlCommand command = new MySqlCommand())
                {
                   
                    command.Connection = connection;
                    command.Parameters.Add("@lbl",MySqlDbType.VarChar).Value = libelle;
                    command.CommandText = query;

                    connection.Open();

                    LogoApp=command.ExecuteReader();    
                    while(LogoApp.Read())
                    {
                        value = LogoApp.GetString(0);
                    }

                    Vue.AffichageContent.OnzeeurLogo(value);
                    connection.Close();
                    LogoApp.Close();

                }
            }
        }

        public static void Inscription(string adresse, string email, string nom, string prenom, int mdp, DateTime ddn, string username)
        {

            using (MySqlConnection connection = new MySqlConnection(ConnexionSQL.ConnStr))
            {
                string query = "INSERT INTO users (`IdUsers`, `FirstName`, `LastName`, `Username`, `Password`, `Adresse`, `email`, `DDN`) VALUES (NULL, @prenom, @nom, @user , @pass, @adress, @email , @ddn)";

                using (MySqlCommand command = new MySqlCommand())
                {

                    command.Connection = connection;
                    command.Parameters.Add("@prenom", MySqlDbType.VarChar).Value = prenom;
                    command.Parameters.Add("@nom", MySqlDbType.VarChar).Value = nom;
                    command.Parameters.Add("@user", MySqlDbType.VarChar).Value = username;
                    command.Parameters.Add("@pass", MySqlDbType.VarChar).Value = mdp;
                    command.Parameters.Add("@adress", MySqlDbType.VarChar).Value = adresse;
                    command.Parameters.Add("@email", MySqlDbType.VarChar).Value = email;
                    command.Parameters.Add("@ddn", MySqlDbType.VarChar).Value = ddn.ToString("yyyy-MM-dd");
                    command.CommandText = query;


                    connection.Open();

                    command.ExecuteNonQuery();

                    connection.Close();

                }
            }
        }


        public static int[] Connexion(string username)
        {
            int[] value= new int[2];
            using (MySqlConnection connection = new MySqlConnection(ConnexionSQL.ConnStr))
            {
                string query = "Select Password, idUsers from users where Username=@username";

                using (MySqlCommand command = new MySqlCommand())
                {

                    command.Connection = connection;
                    command.Parameters.Add("@username", MySqlDbType.VarChar).Value = username;
  
                    command.CommandText = query;


                    connection.Open();

                    connexionMDP = command.ExecuteReader();
                    while (connexionMDP.Read())
                    {

                        value[0]= connexionMDP.GetInt32(0);
                        value[1]= connexionMDP.GetInt32(1);
                    }

                  
                    connection.Close();
                    return value;

                }
            }
        }


        public static List<CarrePlaylist> ListPlyalist(int i, int type, int imageIndex)
        {
            List<CarrePlaylist> CPL = new List<CarrePlaylist>();
            string query;

            using (MySqlConnection connection = new MySqlConnection(ConnexionSQL.ConnStr))
            {
                

                using (MySqlCommand command = new MySqlCommand())
                {
                    switch (type)
                    {
                        case 1:
                            query = "Select b.title, b.id From boxplaylist b Inner Join box_menu bm on(b.id=bm.idBox) Inner Join playlistmenu PL ON(PL.id=bm.idMenu) where PL.id=@id Group by title";
                            break;
                        case 2:
                            query = "Select b.title, b.id From boxplaylist b Inner Join box_menu bm on(b.id=bm.idBox) Inner Join playlistbiblio PB ON(PB.id=bm.idMenu) where PB.id=@id Group by title";
                            break;
                        default:
                            query = "Select b.title, b.id From boxplaylist b Inner Join box_menu bm on(b.id=bm.idBox) Inner Join playlistmenu PL ON(PL.id=bm.idMenu) where PL.id=@id Group by title";
                            break;
                    }

                    command.Connection = connection;
                    command.Parameters.Add("@id", MySqlDbType.VarChar).Value = i;
                    command.CommandText = query;

                    connection.Open();

                    ListCarre = command.ExecuteReader();
                    
                    while (ListCarre.Read())
                    {
                        
                        CPL.Add(new CarrePlaylist(ListCarre.GetString(0), "http://dorianchereault.com/Onzeeur/Images/color/"+ imageIndex + ".png", Convert.ToInt32(ListCarre.GetString(1))));
                    }

                    connection.Close();
                    return CPL;

                }
            }



        }

        public static List<Musique> ListMenu()
        {
            List<Musique> music = new List<Musique>();

            using (MySqlConnection connection = new MySqlConnection(ConnexionSQL.ConnStr))
            {

            string query = "Select PL.libelle, PL.id From playlistmenu PL";

                using (MySqlCommand command = new MySqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = query;

                    connection.Open();

                    ListPlaylist = command.ExecuteReader();
                    while (ListPlaylist.Read())
                    {
                        music.Add(new Musique(ListPlaylist.GetString(0).ToUpper(), ListPlaylist.GetInt32(1),1));
                    }

                    connection.Close();
                    return music;

                }
            }

        }


        public static List<AffichageMusique> ListMusiqueTitres(int id)
        {

            List<AffichageMusique> music = new List<AffichageMusique>();

            using (MySqlConnection connection = new MySqlConnection(ConnexionSQL.ConnStr))
            {

                string query = "Select S.Nom,A.NomArtiste, S.source,S.idSon,S.timecode From sons S Inner Join playlist_son PS on(S.idSon=PS.idSon) Inner join boxplaylist BP ON(PS.idPlaylist=BP.id) INNER join artiste A ON (S.Artiste=A.idArtiste) where BP.id=@id ";

                using (MySqlCommand command = new MySqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = query;
                    command.Parameters.Add(new MySqlParameter("@id", MySqlDbType.Int32)).Value = id;
                    connection.Open();

                    ListMusique = command.ExecuteReader();
                    while (ListMusique.Read())
                    {
                        music.Add(new AffichageMusique(ListMusique.GetString(0), ListMusique.GetString(1), ListMusique.GetString(2), ListMusique.GetInt32(3), ListMusique.GetString(4)));
                    }

                    connection.Close();
                    return music;

                }
            }

        }

        public static List<AffichageMusique> ListMusicSearch(string search)
        {

            if (search==null)
            {
                search = "%";
            }
                List<AffichageMusique> music = new List<AffichageMusique>();

                using (MySqlConnection connection = new MySqlConnection(ConnexionSQL.ConnStr))
                {

                    string query = "Select S.Nom,A.NomArtiste,S.source,S.idSon,S.timecode From sons S inner join artiste A ON(S.Artiste=A.idArtiste) where S.Nom LIKE '" + @search + "%' OR A.NomArtiste LIKE '" + @search + "%' ";

                    using (MySqlCommand command = new MySqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = query;
                        command.Parameters.Add(new MySqlParameter("@search", MySqlDbType.VarChar)).Value = search;
                        connection.Open();

                        ListSearchmusic = command.ExecuteReader();
                        while (ListSearchmusic.Read())
                        {
                        Console.WriteLine(ListSearchmusic.GetString(2));
                        music.Add(new AffichageMusique(ListSearchmusic.GetString(0), ListSearchmusic.GetString(1), ListSearchmusic.GetString(2), ListSearchmusic.GetInt32(3), ListSearchmusic.GetString(4)));
                        }

                        connection.Close();
                        return music;
                    }
                }
        }

        public static List<Musique> ListBibliotheque()
        {
            List<Musique> music = new List<Musique>();

            using (MySqlConnection connection = new MySqlConnection(ConnexionSQL.ConnStr))
            {

                string query = "Select PB.libelle, PB.id From playlistbiblio PB";

                using (MySqlCommand command = new MySqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = query;

                    connection.Open();

                    ListPlaylist = command.ExecuteReader();
                    while (ListPlaylist.Read())
                    {
                        music.Add(new Musique(ListPlaylist.GetString(0).ToUpper(), ListPlaylist.GetInt32(1), 2));
                    }

                    connection.Close();
                    return music;

                }
            }

        }




        public static void Like(int idUser, int idMusique)
        {
            List<CarrePlaylist> CPL = new List<CarrePlaylist>();
            string query;

            using (MySqlConnection connection = new MySqlConnection(ConnexionSQL.ConnStr))
            {


                using (MySqlCommand command = new MySqlCommand())
                {

                    query = "INSERT INTO `like_user` (`id`, `id_sons`, `id_user`) VALUES(NULL, @musique, @user)";
                    command.Connection = connection;
                    command.Parameters.Add("@musique", MySqlDbType.Int32).Value = idMusique;
                    command.Parameters.Add("@user", MySqlDbType.Int32).Value = idUser;
                    command.CommandText = query;

                    connection.Open();

                    command.ExecuteNonQuery();

                    connection.Close();
                    

                }
            }

        }

        public static void UnLike(int idUser, int idMusique)
        {
            List<CarrePlaylist> CPL = new List<CarrePlaylist>();
            string query;

            using (MySqlConnection connection = new MySqlConnection(ConnexionSQL.ConnStr))
            {


                using (MySqlCommand command = new MySqlCommand())
                {

                    query = "DELETE FROM like_user WHERE like_user.id_sons=@musique and like_user.id_user=@user";
                    command.Connection = connection;
                    command.Parameters.Add("@musique", MySqlDbType.Int32).Value = idMusique;
                    command.Parameters.Add("@user", MySqlDbType.Int32).Value = idUser;
                    command.CommandText = query;

                    connection.Open();

                    command.ExecuteNonQuery();

                    connection.Close();


                }
            }

        }

        public static List<AffichageMusique> ListLike(int idUser)
        {

        
            List<AffichageMusique> music = new List<AffichageMusique>();

            using (MySqlConnection connection = new MySqlConnection(ConnexionSQL.ConnStr))
            {
                
                string query = "Select S.Nom,A.NomArtiste,S.source,S.idSon,S.timecode From like_user L INNER JOIN sons S ON(S.idSon=L.id_sons) INNER JOIN artiste A ON(A.idArtiste=S.Artiste)  where L.id_user=@idUser";

                using (MySqlCommand command = new MySqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = query;
                    command.Parameters.Add(new MySqlParameter("@idUser", MySqlDbType.Int32)).Value = idUser;
                    connection.Open();

                    ListSearchmusic = command.ExecuteReader();
                    while (ListSearchmusic.Read())
                    {
                        music.Add(new AffichageMusique(ListSearchmusic.GetString(0), ListSearchmusic.GetString(1), ListSearchmusic.GetString(2), ListSearchmusic.GetInt32(3), ListSearchmusic.GetString(4)));
                    }

                    connection.Close();
                    return music;
                }
            }
        }
    }
}
