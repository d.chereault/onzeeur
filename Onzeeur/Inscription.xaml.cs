﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Net.Mail;

namespace Onzeeur
{
    /// <summary>
    /// Logique d'interaction pour Inscription.xaml
    /// </summary>
    public partial class Inscription : UserControl
    {
        private string ImageRetour = "/Onzeeur; component/Resources/FlecheRetour.png";

        public string GSImageSource
        {
            get => ImageRetour;
            set => ImageRetour = value;
        }

        public int MyProperty { get; set; }

        bool validation = false;

        public Inscription()
        {
            InitializeComponent();
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("http://dorianchereault.com/Onzeeur/Images/FlecheRetour2.png"));
            Cmdback.Background = brush;
            txtPrenom.Text = "";
            Nom.Text = "";
            adresseMail.Text = "";
            txtId.Text = "";
            txtadresse.Text = "";
            txtPassword.Password = "";
            ConfirmPassword.Password = "";
            dateNaissance.Text = "";
            MessageError.Content = "";
            ; }

        private void cmdInscription_Click(object sender, RoutedEventArgs e)
        {
            if (txtadresse.Text != "" || txtPrenom.Text != "" || txtId.Text != "" || ConfirmPassword.Password != "" || txtPassword.Password != "" || dateNaissance.Text != "")
            {
                MessageError.Content = "";
                Firstname = txtPrenom.Text;
                LastName = Nom.Text;
                try
                {
                    DDN = Convert.ToDateTime(dateNaissance.Text);
                    validation = true;
                }
                catch
                {
                    dateNaissance.BorderBrush = System.Windows.Media.Brushes.Red;
                    validation = false;
                }


                if (verifEmail() == true)
                {
                    Email = adresseMail.Text;
                    validation = true;
                }
                else
                {
                    adresseMail.BorderBrush = System.Windows.Media.Brushes.Red;
                }

                if (txtPassword.Password == ConfirmPassword.Password)
                {
                    Password = Convert.ToInt32(txtPassword.Password.GetHashCode());
                    validation = true;
                }
                else
                {
                    txtPassword.BorderBrush = System.Windows.Media.Brushes.Red;
                    ConfirmPassword.BorderBrush = System.Windows.Media.Brushes.Red;
                    validation = false;
                }

                Username = txtId.Text;
                Adresse = txtadresse.Text;

                if (validation == true)
                {

                    try
                    {
                        SqlContent.Inscription(Adresse, Email, LastName, Firstname, Password, DDN, Username);
                        App.Current.MainWindow.Content = new Connexion();


                    }
                    catch (MySqlException f)
                    {

                        MessageBox.Show(f.Message);
                    }
                }


            }
            else
            {
                validation = false;
                MessageError.Content = "Veuillez completer tous les champs";
            }




        }

        public bool verifEmail()
        {
            if (adresseMail.Text.Contains("@"))
            {
                return true;
            } 
            else
            {
                return false;
            }
        }

        private void cmdBack_Click(object sender, RoutedEventArgs e)
        {
            App.Current.MainWindow.Content = new Connexion(); 
        }

        #region

        private string _name;

        public string LastName
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _firstname;

        public string Firstname
        {
            get { return _firstname; }
            set { _firstname = value; }
        }

        private string _username;

        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        private int _password;

        public int Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private string _adresse;

        public string Adresse
        {
            get { return _adresse; }
            set { _adresse = value; }
        }


        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private DateTime _ddn;

        public DateTime DDN
        {
            get { return _ddn; }
            set { _ddn = value; }
        }
        #endregion  
    }
}
