﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using MySql.Data.MySqlClient;
using System.Windows.Shapes;

namespace Onzeeur
{
    /// <summary>
    /// Logique d'interaction pour Connexion.xaml
    /// </summary>
    public partial class Connexion : UserControl
    {
        public Connexion()
        {
            InitializeComponent();
        }

        #region properties
        private static string _user;

        public static string User
        {
            get { return _user; }
            set { _user = value; }
        }

        private int _mdp;

        public int Password
        {
            get { return _mdp; }
            set { _mdp = value; }
        }

        private static int idUser;

        public static int IdUser
        {
            get { return idUser; }
            set { idUser = value; }
        }



        #endregion

        public void SetUserControl(UserControl control)
        {
            control.Width = this.Width;
            control.Height = this.Height;
        }

        private void cmdConnexion_Click(object sender, RoutedEventArgs e)
        {

            User = txtId.Text;
            Password = Convert.ToInt32(txtPassword.Password.GetHashCode());

            try
            {
                int[] currentUser = SqlContent.Connexion(User);
                IdUser = currentUser[1];
                if (Password == currentUser[0])
                    App.Current.MainWindow.Content = new PageAccueil();
                else
                    txtPassword.BorderBrush = System.Windows.Media.Brushes.Red;
            }
            catch (MySqlException f)
            {

                MessageBox.Show(f.Message);
            }
        }
    


            
  

        private void cmdInscription_Click(object sender, RoutedEventArgs e)
        {
            App.Current.MainWindow.Content = new Inscription();
        }

    }
}
