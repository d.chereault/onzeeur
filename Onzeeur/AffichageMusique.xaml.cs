﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Onzeeur
{
    /// <summary>
    /// Logique d'interaction pour AffichageMusique.xaml
    /// </summary>
    public partial class AffichageMusique : UserControl
    {

        public AffichageMusique(string nom, string artiste, string source, int id,string timecode)
        {
            InitializeComponent();
            this.DataContext = this;
            NomArtiste.Text = artiste;
            NomMusique.Text = nom;
            AmNameArtist = artiste;
            AmNameMusic = nom;
            this.id = id;
            this.Source = source;
            timeCode.Text = timecode;

            if (PageAccueil.ToLike == true)
            {
                Logo = "Heart";
            }
            else
            {
                Logo = "CloseCircle";
            }
            
 
        }


        #region properties

        private string _AmNameArtist;
    
            public string AmNameArtist
            { 
                get { return _AmNameArtist; }
                set { _AmNameArtist = value; }
            }

        private string _logo;


            public string Logo
            {
                get { return _logo; }
                set { _logo = value; }
            }

        private string _AmNameMusic;

            public string AmNameMusic
            {
                get { return _AmNameMusic; }
                set { _AmNameMusic = value; }
            }

            private string _AmTimeProgression;

            public string AmTimeProgression
            {
                get { return _AmTimeProgression; }
                set { _AmTimeProgression = value; }
            }

            private string _AmTimeEnd;

            public string AmTimeEnd
            {
                get { return _AmTimeEnd; }
                set { _AmTimeEnd = value; }
            }

        private string _Source;

        public string Source
        {
            get { return _Source; }
            set { _Source = value; }
        }

        private int _id;

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _duration;
  

        public string duration
        {
            get { return _duration; }
            set { _duration = value; }
        }



        #endregion


        public void Click_BtnMusic(object sender, RoutedEventArgs e)
        {
            PageAccueil.PlayingSong = this;
            PageAccueil.NameArtist = AmNameArtist;
            PageAccueil.NameMusic = AmNameMusic;
            PageAccueil.Instance.update(Source);
        }

        public void Click_Fav(object sender, RoutedEventArgs e)
        {
            if (Logo == "Heart")
                SqlContent.Like(Connexion.IdUser, this.id);
            else
            {
                SqlContent.UnLike(Connexion.IdUser, this.id);
                PageAccueil.Instance.MenuBibliotheque(); ;
            }
        }


    }
}
