﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Media.Animation;

namespace Onzeeur
{
    /// <summary>
    /// Logique d'interaction pour Musique.xaml
    /// </summary>
    public partial class Musique : UserControl
    {
        public Musique(string titre, int id, int type)
        {
            InitializeComponent();
            this.DataContext = this;
            TitlePL.Text = titre;
            Id = id;
            this.type = type;

            GenList();
        }


        #region properties
            private static string _titleList;

            private int _id;

            public int Id
            {
                get { return _id; }
                set { _id = value; }
            }

            public static string TitleList
            {
                get { return _titleList; }
                set { _titleList = value; }
            }



            private int _type;

            public int type
            {
                get { return _type; }
                set { _type = value; }
            }
        #endregion


        private void GenList()
        {
            Random aleatoire = new Random();
            int imageIndex = aleatoire.Next(400);
            System.Threading.Thread.Sleep(50);
            ListPL.ItemsSource = SqlContent.ListPlyalist(Id, type, imageIndex);
        }

        private void Click_BtnNext(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                LeftRight.LineRight();
            }
        }


        private void Click_BtnPrev(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                LeftRight.LineLeft();
            }
        }


    }
}
    