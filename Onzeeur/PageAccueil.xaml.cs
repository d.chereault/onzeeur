﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using System.Timers;
using System.Windows.Threading;
using WMPLib;

namespace Onzeeur
{
    /// <summary>
    /// Logique d'interaction pour PageAccueil.xaml
    /// </summary>
    public partial class PageAccueil : UserControl
    {
        
        public PageAccueil()
        {
            InitializeComponent();
            this.DataContext = this;
            _obj = this;
            mediaPlayer.MediaOpened += mediaPlayer_MediaOpened;
            mediaPlayer.MediaEnded += new EventHandler(Media_Ended_Change);



            try
            {
                SqlContent.Image("LogoApp");

            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
            }

            ListMenu.ItemsSource = SqlContent.ListMenu();
            
        }


        #region properties

        static List<AffichageMusique> listMusic = new List<AffichageMusique>();
        DispatcherTimer timer = new DispatcherTimer();
        MediaPlayer mediaPlayer = new MediaPlayer();
        bool positionPlay = true;
        bool positionVolume = true;
        double VolumeHight=0.50;
        bool Repeat=false;

        private static bool _toLike = true;

        public static bool ToLike
        {
            get { return _toLike; }
            set { _toLike = value; }
        }


        static private string _LogoApp;

        public static string LogoApp
        {
            get { return _LogoApp; }
            set { _LogoApp = value; }
        }

        static PageAccueil _obj;

        public static AffichageMusique PlayingSong { get; set; }

        public static PageAccueil Instance  
        {
            get
            {
                if (_obj == null)
                {
                    _obj = new PageAccueil();
                }
                return _obj;
            }
        }

        public ItemsControl Menu
        {
            get { return ListMenu; }
            set { ListMenu = value; }
        }


        private string _duration;

        public string duration
        {
            get { return _duration; }
            set { _duration = value; }
        }

        private static string _NameMusic;

        public static string NameMusic
        {
            get { return _NameMusic; }
            set { _NameMusic = value; }
        }

        private static string _NameArtist;

        public static string NameArtist
        {
            get { return _NameArtist; }
            set { _NameArtist = value; }
        }

        private static string _TimeActual;

        public static string TimeActual
        {
            get { return _TimeActual; }
            set { _TimeActual = value; }
        }

        private static string _TimeEnd;

        public static string TimeEnd
        {
            get { return _TimeEnd; }
            set { _TimeEnd = value; }
        }

        public static void setListMusic(List<AffichageMusique> musicPlaylist)
        {
            listMusic = musicPlaylist;
        }
        #endregion

        private void SearchBar_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void BtnMenuMusique(object sender, RoutedEventArgs e)
        {
            ToLike = true;
            ListMenu.ItemsSource = SqlContent.ListMenu();
        }


        private void Click_BtnPlay(object sender, RoutedEventArgs e)
        {
            if (mediaPlayer.Source != null)
            {
                if (positionPlay == true)
                {
                    timer.Stop();
                    mediaPlayer.Pause();
                    btnPlayIco.Kind = MaterialDesignThemes.Wpf.PackIconKind.Play;
                    positionPlay = false;
                }
                else
                {
                    timer.Start();
                    mediaPlayer.Play();
                    btnPlayIco.Kind = MaterialDesignThemes.Wpf.PackIconKind.Pause;
                    positionPlay = true;
                }

            }
        }

        public void update(string source)
        {
            mediaPlayer.Stop();
            NomMusique.Text = NameMusic;
            NomArtiste.Text = NameArtist;
            Seperation.Text = "-";
            mediaPlayer.Open(new Uri(source));
            
            btnPlayIco.Kind = MaterialDesignThemes.Wpf.PackIconKind.Pause;
            mediaPlayer.Volume = (double)VolumeSlider.Value / 100;
            
            mediaPlayer.Play();

        }

        private void Media_Ended(object sender, EventArgs e)
        {
            if (mediaPlayer.Source != null)
            {
                mediaPlayer.Position = TimeSpan.Zero;
                mediaPlayer.Play();
            }
        }

        private void Media_Ended_Change(object sender, EventArgs e)
        {
            try
            {
                AffichageMusique Next = listMusic.Where(x => x.Source == mediaPlayer.Source.ToString()).First();
                Next = listMusic[(listMusic.FindIndex(x => x.Source == mediaPlayer.Source.ToString()) + 1) % listMusic.Count()];
                if(Next!=PlayingSong)
                {
                    Next.Click_BtnMusic(new object(), new RoutedEventArgs());
                }
                
            }
            catch (Exception error)
            {
                Console.WriteLine("error :" + error.Message);
            }
           

            
        }


        private void mediaPlayer_MediaOpened(object sender, EventArgs e)
        {
            
            Duration.Text= TimeSpan.FromSeconds(mediaPlayer.NaturalDuration.TimeSpan.TotalSeconds).ToString(@"mm\:ss");
            Progression.Text = TimeSpan.FromSeconds(0).ToString(@"mm\:ss");
            PlayingSong.AmTimeEnd = TimeSpan.FromSeconds(mediaPlayer.NaturalDuration.TimeSpan.TotalSeconds).ToString(@"mm\:ss");

            progressBar.Maximum = mediaPlayer.NaturalDuration.TimeSpan.TotalSeconds;

            timer.Interval = TimeSpan.FromSeconds(0.5);
            timer.Tick += OnTimedEvent;
            timer.Start();

        }

        private void OnTimedEvent(object sender, EventArgs e)
        {
            progressBar.Value = Convert.ToDouble(mediaPlayer.Position.TotalSeconds);
            Progression.Text= TimeSpan.FromSeconds(progressBar.Value).ToString(@"mm\:ss");
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
           
            mediaPlayer.Volume = (double)VolumeSlider.Value/100;

            if(mediaPlayer.Volume==0)
            {
                btn_SoundIco.Kind = MaterialDesignThemes.Wpf.PackIconKind.Mute;
            }
            else
            {
                if(mediaPlayer.Volume>0.66)
                    btn_SoundIco.Kind = MaterialDesignThemes.Wpf.PackIconKind.VolumeHigh;
                if(mediaPlayer.Volume<=0.66 && mediaPlayer.Volume >= 0.33)
                    btn_SoundIco.Kind = MaterialDesignThemes.Wpf.PackIconKind.VolumeMedium;
                if (mediaPlayer.Volume <= 0.33 && mediaPlayer.Volume >= 0)
                    btn_SoundIco.Kind = MaterialDesignThemes.Wpf.PackIconKind.VolumeLow;
            }
        }

        private void Click_BtnMute(object sender, RoutedEventArgs e)
        {

            if (positionVolume == false)
            {
                mediaPlayer.Volume = VolumeHight;
                VolumeSlider.Value = VolumeHight*100;
                if (VolumeHight > 0.66)
                    btn_SoundIco.Kind = MaterialDesignThemes.Wpf.PackIconKind.VolumeHigh;
                if (VolumeHight <= 0.66 && mediaPlayer.Volume >= 0.33)
                    btn_SoundIco.Kind = MaterialDesignThemes.Wpf.PackIconKind.VolumeMedium;
                if (VolumeHight <= 0.33 && mediaPlayer.Volume >= 0)
                    btn_SoundIco.Kind = MaterialDesignThemes.Wpf.PackIconKind.VolumeLow;
                positionVolume = true;
            }
            else
            {
                VolumeHight = mediaPlayer.Volume;
                mediaPlayer.Volume = 0;
                VolumeSlider.Value = 0;
                btn_SoundIco.Kind = MaterialDesignThemes.Wpf.PackIconKind.Mute;
                positionVolume = false;
                
            }
        }

        private void Click_BtnLoop(object sender, RoutedEventArgs e)
        {
            if(mediaPlayer.Source!=null)
            {
                if (Repeat==false)
                {
                    BtnLoopIco.Kind = MaterialDesignThemes.Wpf.PackIconKind.Repeat;
                    mediaPlayer.MediaEnded -= new EventHandler(Media_Ended_Change);
                    mediaPlayer.MediaEnded += new EventHandler(Media_Ended);
                    Repeat = true;
                }
                else
                {
                    BtnLoopIco.Kind = MaterialDesignThemes.Wpf.PackIconKind.RepeatOff;
                    mediaPlayer.MediaEnded -= new EventHandler(Media_Ended);
                    mediaPlayer.MediaEnded += new EventHandler(Media_Ended_Change);
                    Repeat = false;
                }

                mediaPlayer.MediaOpened += new EventHandler(mediaPlayer_MediaOpened);
            }
        }
        private void Click_BtnNext(object sender, RoutedEventArgs e)
        {
            if(mediaPlayer.Source!=null)
            {
                mediaPlayer.Position = mediaPlayer.NaturalDuration.TimeSpan;
                if(positionPlay==false)
                    Click_BtnPlay(sender, e);
            }
        }

        private void Click_BtnPrevious(object sender, RoutedEventArgs e)
        {
            if (mediaPlayer.Source != null)
            {
                
                mediaPlayer.Position = TimeSpan.Parse("0");
                if (positionPlay == false)
                    Click_BtnPlay(sender, e);
            }
        }


        private void SearchBar_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            listMusic= SqlContent.ListMusicSearch(SearchBar.Text);
            ListMenu.ItemsSource = listMusic;
        }

        private void ProgressBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            mediaPlayer.Position = TimeSpan.FromSeconds(progressBar.Value);
            Progression.Text = TimeSpan.FromSeconds(progressBar.Value).ToString(@"mm\:ss");
            mediaPlayer.Play();


        }

        private void BtnMenuDecouvrir(object sender, RoutedEventArgs e)
        {
            ToLike = true;
            ListMenu.ItemsSource = SqlContent.ListBibliotheque();
        }

        private void BtnMenuBibliotheque(object sender, RoutedEventArgs e)
        {
            ToLike = false;
            ListMenu.ItemsSource = SqlContent.ListLike(Connexion.IdUser);
        }

        public void MenuBibliotheque()
        {
            ToLike = false;
            ListMenu.ItemsSource = SqlContent.ListLike(Connexion.IdUser);
        }

        private void Click_Deconnexion(object sender, RoutedEventArgs e)
        {
            ToLike = true;
            App.Current.MainWindow.Content = new Connexion();
        }
    }
}
