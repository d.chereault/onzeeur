Onzeeur - Epreuve E4
==============================================

L'application
-------------


### Présentation

La nouvelle entreprise Onzeeur, au nombre de six salariés dont un Community manager, un chargé de communication, deux développeurs et un designer, a été créée en 2020. Lionel MARSEAU préside avec Pierre LABLANCHE cette société anonyme, qui a pour but la conception d’une application Windows permettant d’écouter de la musique sans téléchargement et de manière gratuite


### Fonctionnalités

- [ ] Se connecter via un identifiant (exemple : pseudonyme / mot de passe)
- [ ] Application accessible seulement aux membres
- [ ] La solution doit permettre de lire de la musique, de la mettre en pause et de la mettre en boucle
- [ ] L'application doit mettre à disposition des playlists en lecture automatique
- [ ] L'application doit permettre une recherche simple et efficace d'une musique ou d'un auteur
- [ ] Elle doit proposer à l'utilisateur de se rappeler des musiques que ce dernier souhaite
- [ ] L'application permet de régler le voulume de la musique et permet de l'écouter en arrière plan


### Technologies

*   Gestionnaire de version : **GIT** 
*   Librairies externes (exemple : materialDesign)
*   Framework .NET
*   Langage de programmation : C#
*   Concepteur de forms : WPF 
  
  
### Mise en route

*   ![](https://a.slack-edge.com/production-standard-emoji-assets/13.0/google-medium/1f4da.png) Ouvrir le fichier Onzeeur.sln avec Visual Studio 2017
*   ![](https://a.slack-edge.com/production-standard-emoji-assets/13.0/google-medium/1f3d7-fe0f.png) Appuyer sur F5
*   ![](https://a.slack-edge.com/production-standard-emoji-assets/13.0/google-medium/1f4d1.png) Identifiants d'exemple -> identifiant : admin || mot de passe : admin


### Compte FTP du stockage de ressource
Voir fiche documentation 

### Base de données
Nom de la base : Onzeeur
  
